<?php

require_once('controller/ShoppingCart.php');
require_once('controller/Item.php');

$item = new Item("corn flakes", 500, 6000);
var_dump($item->getPrice());
var_dump($item->getName());
var_dump($item->getWeight());
echo nl2br($item->getName() . ":" . $item->getPrice() . "€ \r\n");

$chewingGum = new Item("chewingGum", 453, 8008);
echo nl2br($chewingGum->getName() . ":" . $chewingGum->getPrice() . "€ \r\n");
$panier = new Panier();
$panier->addItem($item);
$panier->addItem($chewingGum);
$panier->itemCount();
$panier->totalPrice();
