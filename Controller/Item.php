<?php

class Item{
    private String $name;
    private Float $price;
    private Int $weight;

    public function __construct($newName, $newPrice, $newWeight) {
        $this->setName($newName);
        $this->setPrice($newPrice);
        $this->setWeight($newWeight);

    }

    public function setName($newName){
        $this->name = $newName;
    }

    public function getName(){
        return $this->name;
    }

    public function setPrice($newPrice){
        $this->price = $newPrice;
    }

    public function getPrice(){
        return substr_replace($this->price, '.', -2, 0);
    }
    public function setWeight($newWeight){
        $this->weight = $newWeight;
    }

    public function getWeight(){
        return $this->weight;
    }
}
