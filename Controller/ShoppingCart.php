<?php

require_once('Item.php');

class Panier {
    private $items = array();


    public function addItem($newItem){
        if ($this->getTotalWeight() > 10000 || ($this->getTotalWeight() + $newItem->getWeight()) > 10000) {
            echo nl2br("Aie Aie \r\n");
        }
        else {
            $this->items[] = $newItem;
            echo nl2br("L'article a bien été ajouté au panier \r\n");
        }    
    }

    public function removeItem($item){
        $itemToRemove = array_search($item, $this->items);
       if ($itemToRemove !== false) {
         unset($this->items[$itemToRemove]);
         echo nl2br("L'élément " . $item->getName() . " a été retiré du panier \r\n");
       }
       else {
        return false;
       }
    }

    public function itemCount(){
        $nbItems = count($this->items);
        echo  nl2br("Il y a " . $nbItems . " article dans le panier \r\n") ;
    }
    public function totalPrice() {
        $newPrice = 0;
        foreach ($this->items as $item) {
           $newPrice = $newPrice + $item->getPrice();
        }
        echo "Le prix total du panier est de " . number_format($newPrice, 2, '.') . "€";
        return $newPrice;

    }
    public function getTotalWeight(){
        $newWeight = 0;
        foreach ($this->items as $item) {
           $newWeight = $newWeight + $item->getWeight();
        }
        return $newWeight;
    }
}
// $item = new Item("corn flakes", 500);
// var_dump($item->getPrice());
// var_dump($item->getName());
// echo "<pre>"; var_dump($item->getName()) ; echo ":"; var_dump($item->getPrice()); echo "€</pre>";
